/*
 * Copyright (C) 2018,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#include "json.h"
#include "jwt.h"
#include "base64url.h"

static char *hmac_sign(gnutls_mac_algorithm_t mac,
								const char *data, const char *key)
{
	gnutls_hmac_hd_t handle;
	char *bufp;

	if (gnutls_hmac_init(&handle, mac, key, key ? strlen(key) : 0) < 0)
		return NULL;

	if (!(bufp = calloc(1, gnutls_hmac_get_len(mac) + 1)))
		return NULL;

	if (gnutls_hmac(handle, data, strlen(data)) < 0)
		return NULL;

	gnutls_hmac_deinit(handle, bufp);
	return bufp;
}


char *jwt_sign(char *dst, size_t size, 
							const char *header, const char *claims, 
							const char *key) 
{
	struct json *h;
	const char *alg;
	char *p, *bufp;

	if (!(h = json_from_buf((char *)header, strlen(header), NULL, 0)))
		return NULL;

	if (!json_find_string(h, "alg", &alg))
		return NULL;

	if (!(p = base64url_encode(dst, header, size)))
		return NULL;

	if (!base64url_encode(dst+strlen(p) + 1, claims, size - strlen(p)))
		return NULL;

	p[strlen(p)] = '.';

	if (strcmp(alg, "HS256") == 0) {
		bufp = hmac_sign(GNUTLS_MAC_SHA256, p, key);
		if (!(bufp &&
					base64url_encode(dst + strlen(p) + 1, bufp, size - strlen(p))))
			return NULL;
		free(bufp);
		p[strlen(p)] = '.';
	}
	else if (strcmp(alg, "none") == 0) {
		strcat(p, ".");
	}
	else
		return NULL;

	return dst;
}
