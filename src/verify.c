/*
 * Copyright (C) 2018,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#include <json.h>
#include "jwt.h"
#include "base64url.h"

int jwt_verify(const char *token, const char *alg, const char *key) {
	char buf[4096], sbuf[512], ebuf[256];
	struct json *header;
	char *p, *h, *s;
	const char *fake_alg, *typ;
	int dot, ret = -1;
	gnutls_mac_algorithm_t mac;
	gnutls_hmac_hd_t handle;
	
	if (!(h = malloc(strlen(token) + 1)))
		return -1;
	
	strcpy(h, token);

	/*
	 * Check the token external structure
	 * Prepare next checks
	 */
	p = s = h;
	dot = 0;
	while (*p) {
		if (*p == '.') {
			if (dot == 1) {
				*p++ ='\0';
				s = p++;
			}
			else
				p++;
			dot++;
		}
		else 
			p++;
	}

	if (dot != 2) {
		ret = -2;
		goto fails;
	}

	/*
	 * Grab the header
	 */
	if (!base64url_decode(buf, h, sizeof buf)) {
		ret = -3;
		goto fails;
	}
	if (!(header = json_from_buf(buf, strlen(buf), ebuf, sizeof ebuf))) {
		ret = -4;
		goto fails;
	}
	if (strcmp(alg, "HS256") == 0) {
		mac = GNUTLS_MAC_SHA256;
		if (!(json_find_string(header, "alg", &fake_alg) &&
					strcmp(alg, fake_alg) == 0 &&
				  json_find_string(header, "typ", &typ) &&
				  strcmp(typ, "JWT") == 0)) {
			ret = -5;
			goto fails;
		}
	}
	else {
		ret = -6;
		goto fails;
	}

	/*
	 * Check signature
	 */

	if (!base64url_decode(sbuf, s, sizeof sbuf)) {
		ret = -8;
		goto fails;
	}
	if (gnutls_hmac_init(&handle, mac, key, key ? strlen(key) : 0) < 0) {
		ret = -9;
		goto fails;
	}
	gnutls_hmac(handle, h, strlen(h));
	memset(buf, 0, sizeof buf);
	gnutls_hmac_deinit(handle, buf);
	if (strcmp(buf, sbuf) != 0) {
		ret = -10;
		goto fails;
	}
	return 0;
fails:
	free(h);
	return ret;
}
