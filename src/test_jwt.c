/*
 * Copyright (C) 2018,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <json.h>
#include "jwt.h"
#include "base64url.h"

/*
 * The JWT RFC sample!
 */

static char header[] = "{\"typ\":\"JWT\",\r\n \"alg\":\"HS256\"}";
static char claims[] = 
	"{\"iss\":\"joe\",\r\n \"exp\":1300819380,\r\n"
	" \"http://example.com/is_root\":true}";

static char base64_secret[] = 
	"AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1q"
	"S0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow";

static char valid_result[] =
	"eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9"
	"."
	"eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogIm"
	"h0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ"
	"."
	"dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk";

/*
 * alg = "none", same claims
 */

static char header_none[] = "{\"alg\":\"none\"}";
static char valid_result_none[] =
	"eyJhbGciOiJub25lIn0"
  "."
	"eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogIm"
	"h0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ"
  "."; 

int main(void) {
	char buf[4196];
	char *result;
	char secret[2048];

	/*
	 * JWT RFC Sample.
	 */

	if (!base64url_decode(secret, base64_secret, sizeof secret)) {
			puts("invalid key decode");
			return 0;
	}

	puts("RFC Sample (HS256)");
	result = jwt_sign(buf, sizeof buf, header, claims, secret);
	if (result) {
		printf("%s\n", result);
		if (strcmp(result, valid_result) == 0) {
			int ret;
			struct json *header, *claims;
			
			puts("valid result");
			ret = jwt_info(result, &header, &claims);
			if (ret == 0) {
				puts("valid info");
				puts("header:");
				json_to_file(header, stdout);
				puts("\nclaims:");
				json_to_file(claims, stdout);
				putchar('\n');
				ret = jwt_verify(result, "HS256", secret);
				if (ret == 0)
					puts("valid check");
				else
					printf("invalid check, ret=%d\n", ret);
			}
			else {
				printf("invalid info, ret=%d\n", ret);
			}
		}
		else
			puts("invalid result");
	}
	else 
		puts("null result");

	/*
	 * alg="none"
	 */

	puts("RFC Sample (none)");
	result = jwt_sign(buf, sizeof buf, header_none, claims, NULL);
	if (result) {
		printf("%s\n", result);
		if (strcmp(result, valid_result_none) == 0) {
			int ret;
			struct json *header, *claims;
			
			puts("valid result");
			ret = jwt_info(result, &header, &claims);
			if (ret == 0) {
				puts("valid info");
				puts("header:");
				json_to_file(header, stdout);
				puts("\nclaims:");
				json_to_file(claims, stdout);
				putchar('\n');
				ret = jwt_verify(result, "none", secret);
				if (ret == 0)
					puts("valid check");
				else
					printf("invalid check, ret=%d\n", ret);
			}
			else {
				printf("invalid info, ret=%d\n", ret);
			}
		}
		else
			puts("invalid result");
	}
	else 
		puts("null result");

	return 0;
}
