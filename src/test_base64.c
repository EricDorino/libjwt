/*
 * Copyright (C) 2018,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "base64url.h"

static void run_enc_dec(const char *str) {
	char buf[2048], result[2048];

	printf("encode decode \"%s\"\n", str);

	if (!base64url_encode(buf, str, sizeof buf)) {
			puts("invalid encode");
			return;
	}

	if (!base64url_decode(result, buf, sizeof result)) {
			puts("invalid decode");
			return;
	}

	printf("\"%s\"\n", result);
	if (strcmp(result, str) == 0)
		puts("valid result");
	else
		puts("invalid result");
}

static void run_dec_enc(const char *str) {
	char buf[2048], result[2048];

	printf("decode encode \"%s\"\n", str);

	if (!base64url_decode(buf, str, sizeof buf)) {
			puts("invalid encode");
			return;
	}

	if (!base64url_encode(result, buf, sizeof result)) {
			puts("invalid decode");
			return;
	}

	printf("\"%s\"\n", result);
	if (strcmp(result, str) == 0)
		puts("valid result");
	else
		puts("invalid result");
}

int main(void) {
	char str0[] = "This is a string, 565££$*$ù%!§";
	char str1[] = 
"AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow";

	run_enc_dec(str0);
	run_enc_dec(str1);

	run_dec_enc(str1);

	return 0;
}
