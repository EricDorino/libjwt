/*
 * Copyright (C) 2018,  Eric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef jwt_h_
#define jwt_h_
#include <json.h>

extern char *jwt_sign(char *, size_t,
								const char *header, const char *claims,
								const char *key);
extern int jwt_info(const char *token,
								struct json **header, struct json **claims);
extern int jwt_verify(const char *token, const char *alg, const char *key);

#endif
