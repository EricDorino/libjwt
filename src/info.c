/*
 * Copyright (C) 2018,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#include <json.h>
#include "jwt.h"
#include "base64url.h"

int jwt_info(const char *token, struct json **header, struct json **claims) {
	char buf[4096], ebuf[256];
	char *p, *h, *c, *s;
	int dot, ret;
	
	if (!(h = malloc(strlen(token) + 1)))
		return -1;
	
	strcpy(h, token);

	/*
	 * Check the token external structure
	 * Prepare next checks
	 */
	p = c = s = h;
	dot = 0;
	while (*p) {
		if (*p == '.') {
			*p++ ='\0';
			if (dot == 0)
				c = p++;
			else
				s = p++;
			dot++;
		}
		else 
			p++;
	}

	if (dot != 2) {
		ret = -2;
		goto fails;
	}

	/*
	 * Check the header
	 */
	if (!base64url_decode(buf, h, sizeof buf)) {
		ret = -3;
		goto fails;
	}
	if (!(*header = json_from_buf(buf, strlen(buf), ebuf, sizeof ebuf))) {
		ret = -4;
		goto fails;
	}

	/*
	 * Get the claims
	 */
	if (!base64url_decode(buf, c, sizeof buf)) {
		ret = -5;
		goto fails;
	}
	if (!(*claims = json_from_buf(buf, strlen(buf), ebuf, sizeof ebuf))) {
		ret = -6;
		goto fails;
	}

	return 0;
fails:
	free(h);
	return ret;
}
