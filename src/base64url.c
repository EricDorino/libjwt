/*
 * Copyright (C) 2018,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include "base64url.h"

char *base64url_encode(char *dst, const char *src, size_t size)
{
	static const char base64[] =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
	unsigned char in[3] = {0, 0, 0};
	unsigned char out[4] = {0, 0, 0, 0};
	int i, j, index;
	size_t srclen, len;
	char *p, *plast;

	if (!src)
		return NULL;
	srclen = strlen(src);
	len = 4 * srclen / 3 + (srclen % 3 ? 4 : 0) + 2 * ((srclen / 76) + 1);
	
	if (size < len)
		return NULL;

	for (plast = (char *)src; *plast; plast++);
	plast--;

	for (i = 0, j = 0, p = (char *)src; p <= plast; i++, p++) {
		index = i % 3;
		in[index] = *p;

		if (index == 2 || p == plast) {
			out[0] = ((in[0] & 0xfc) >> 2);
			out[1] = ((in[0] & 0x3) << 4) | ((in[1] & 0xf0) >> 4);
			out[2] = ((in[1] & 0xf) << 2) | ((in[2] & 0xc0) >> 6);
			out[3] = (in[2] & 0x3f);

			dst[j++] = base64[out[0]];
			dst[j++] = base64[out[1]];
			if (index != 0)
				dst[j++] = base64[out[2]];
			if (index >= 2)
				dst[j++] = base64[out[3]];
			in[0] = in[1] = in[2] = 0;
		}
	}

	dst[j] = '\0';
	return dst;
}

#define is_upper(c) ((c)>='A' && (c)<='Z')
#define is_lower(c) ((c)>='a' && (c)<='z')
#define is_digit(c) ((c)>='0' && (c)<='9')

char *base64url_decode(char *dst, const char *src, size_t size)
{
	unsigned char in[4] = {0, 0, 0, 0};
	unsigned char out[3] = {0, 0, 0};
	int i, j, index;
	char *p;
	const char *plast;

	if (!src)
		return NULL;

	for (plast = (char *)src; *plast != '=' && *plast != '\0'; plast++);
	plast--;

	for (p = (char *)src, i = 0, j = 0; p <= plast; p++, i++) {
		index = i % 4;

		if (is_upper(*p)) in[index] = *p - 65;
		else if (is_lower(*p)) in[index] = *p - 71;
		else if (is_digit(*p)) in[index] = *p + 4;
		else if (*p == '-') in[index] = 62;
		else if (*p == '_') in[index] = 63;
		else in[index] = 0;

		if (index == 3 || p == plast) {
			out[0] = (in[0] << 2) | ((in[1] & 0x30) >> 4);
			out[1] = ((in[1] & 0x0f) << 4) | ((in[2] & 0x3c) >> 2);
			out[2] = ((in[2] & 0x03) << 6) | (in[3] & 0x3f);

			if (j >= (int)size - 4)
				break;
			if (p == plast) {
				dst[j++] = out[0];
				index >= 2 ? dst[j++] = out[1] : 0;
				index == 3 ? dst[j++] = out[2] : 0;
			}
			else {
				dst[j++] = out[0];
				dst[j++] = out[1];
				dst[j++] = out[2];
			}
			out[0] = out[1] = out[2] = 0;
			in[0] = in[1] = in[2] = in[3] = 0;
		}
	}
	dst[j] = '\0';

	return dst;
}
